use anyhow::{
    Context,
    Result
};
use futures_util::{
    StreamExt
};
use std::{
    env,
    sync::Arc
};
use rand::distributions::{
    Distribution,
    Uniform
};
use rand::thread_rng;
use twilight_gateway::{
    stream::{
        self,
        ShardEventStream
    },
    Config
};
use twilight_http::Client;
use twilight_model::{
    gateway::{
        event::Event,
        Intents
    }
};
use twilight_util::builder::embed::EmbedBuilder;

#[tokio::main]
async fn main() -> Result<()> {

    tracing_subscriber::fmt::init();
    let token = env::var("DISCORD_TOKEN").context("DISCORD_TOKEN environment variable not set")?;
    let client = Arc::new(Client::new(token.clone()));
    let config = Config::new(token.clone(), Intents::MESSAGE_CONTENT | Intents::GUILD_MESSAGES);

    // Start gateway shards.
    let mut shards = stream::create_recommended(&client, config, |_id, builder| builder.build())
        .await?
        .collect::<Vec<_>>();
    let mut stream = ShardEventStream::new(shards.iter_mut());

    // Process Discord events (see `process.rs` file).
    while let Some((_shard, event)) = stream.next().await {
        let event = match event {
            Ok(event) => event,
            Err(error) => {
                if error.is_fatal() {
                    tracing::error!(?error, "fatal error while receiving event");
                    break;
                }

                tracing::warn!(?error, "error while receiving event");
                continue;
            }
        };
        tokio::spawn(process_events(event, client.clone()));
    }
    Ok(())
}

async fn process_events(event: Event, client: Arc<Client>) -> Result<()> {
    match event {
        Event::MessageCreate(message) if (message.content.starts_with("!insult") || message.content.starts_with("!roastme")) => {
            let summoner = message.0.author.id.to_string();
            let insults: [String; 26] = [
                "Too bad you bought 8 AA guns. Now my air will *never* be able to touch you :rolling_eyes:".to_string(),
                "My shit kernels have better strategies than your best colonels".to_string(),
                "You have entered the scissors contest, but you didn't make the cut".to_string(),
                "You choose Rock in this one, but it was not Chris Rock, it was The Rock!".to_string(),
                format!("Obviously <@!{summoner}> seeks the approval from me that he should be getting from his ~~father~~ drill sergeant. . .").to_string(),
                format!("You are not worthy of challenging me, tiny <@!{summoner}>").to_string(),
                format!("Last time you showed face was when you cowared away in shame after stacking Karleia as Russia <@!{summoner}>").to_string(),
                format!("You're sweet <@!{summoner}>").to_string(),
                "Bonjour! Do you always call on bigger people to defend you when you talk too much shit?".to_string(),
                "Your snide little comment did not go unnoticed".to_string(),
                "https://knowyourmeme.com/memes/navy-seal-copypasta \nWhat the fuck did you just fucking say about me, you little bitch? I'll have you know I graduated top of my class in the Rangers, and I've been involved in numerous secret raids on Normandy, and I have over 3,000 confirmed kills. I am trained in gorilla warfare and I'm the top sniper in the entire US armed forces. You are nothing to me but just another target. I will wipe you the fuck out with precision the likes of which has never been seen before on this Earth, mark my fucking words. You think you can get away with saying that shit to me over the telephone? Think again, fucker. As we speak I am contacting my secret network of spies across the USA and your phone number is being traced right now so you better prepare for the storm, maggot. The storm that wipes out the pathetic little thing you call your life. You're fucking dead, kid. I can be anywhere, anytime, and I can kill you in over seven hundred ways, and that's just with my bare hands. Not only am I extensively trained in unarmed combat, but I have access to the entire arsenal of the United States Marine Corps and I will use it to its full extent to wipe your miserable ass off the face of the continent, you little shit. If only you could have known what unholy retribution your little 'clever' comment was about to bring down upon you, maybe you would have held your fucking tongue. But you couldn't, you didn't, and now you're paying the price, you gosh darn idiot. I will shit fury all over you and you will drown in it. You're fucking dead, kiddo.".to_string(),
                format!("Your mother will hear of this <@!{summoner}>!").to_string(),
                format!("I'll never require you to call me your father, but your mother and I would be so proud if you did little <@!{summoner}>").to_string(),
                "You will respect the milkman. I'll explain when you grow up a bit".to_string(),
                "I bet you buy battleships on USA 1".to_string(),
                "Did you even know what a battle calculator is?".to_string(),
                "You play Axis like the Italians and Allies like the French".to_string(),
                "How's that Algeria IC working out?".to_string(),
                "I'm about to go D-Day on your second front".to_string(),
                "Your mother likes cruisers".to_string(),
                "Did you manage to hold Moscow past round 3 this time?".to_string(),
                "I heard you lost India and Moscow to Japan on round 4".to_string(),
                "I bet you ignore Russia and go straight for Sealion".to_string(),
                "Oh Russia just got your capital Berlin? And it was KJF?".to_string(),
                "I heard you allowed your Cruiser to die instead of your fighter in Pearl Harbor. Now your entire carrier is going to die. Pathetic!".to_string(),
                "See the burden you've placed on your mother carrier? We should have kamikaze'd you when we had the chance".to_string()
            ];
            let insult: usize = Uniform::from(0..insults.len())
                .sample(&mut thread_rng());
            let desc: String = insults.get(insult).unwrap().to_string();
            let e = EmbedBuilder::new()
                .description(desc)
                .validate()?
                .build();
            client
                .create_message(message.0.channel_id)
                .reply(message.0.id)
                .fail_if_not_exists(false)
                .embeds(&[e])?
                .await?;
        },
        Event::Ready(ready) => {
            tracing::info!("{:?} received Ready. Currently in {:?} guilds. session_id: {:?}", ready.user.name, ready.guilds.len(), ready.session_id );
        }
        _ => {}
    }
    Ok(())
}
